﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Development.Models;

namespace Development.Controllers
{
    public partial class AchievedSkillController : Controller
    {
        private DevelopmentContext db = new DevelopmentContext();

        //
        // GET: /achievedSkill/

        public virtual ViewResult Index()
        {
            var achievedskills = db.AchievedSkills.Include(a => a.employee).Include(a => a.skill).Include(a => a.skillLevel).Include(a => a.skillScore).Include(a => a.ApprovedBy);
            return View(achievedskills.ToList());
        }

        //
        // GET: /achievedSkill/Details/5

        public virtual ViewResult Details(int id)
        {
            AchievedSkill achievedskill = db.AchievedSkills.Find(id);
            return View(achievedskill);
        }

        //
        // GET: /achievedSkill/Create

        public virtual ActionResult Create()
        {
            ViewBag.employeeId = new SelectList(db.Employees, "id", "firstName");
            ViewBag.skillId = new SelectList(db.Skills, "id", "name");
            ViewBag.skillLevelId = new SelectList(db.SkillLevels, "id", "name");
            ViewBag.skillScoreId = new SelectList(db.SkillScores, "id", "name");
            ViewBag.approvedById = new SelectList(db.Employees, "id", "firstName");
            return View();
        } 

        //
        // POST: /achievedSkill/Create

        [HttpPost]
        public virtual ActionResult Create(AchievedSkill achievedskill)
        {
            if (ModelState.IsValid)
            {
                db.AchievedSkills.Add(achievedskill);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.employeeId = new SelectList(db.Employees, "id", "firstName", achievedskill.EmployeeId);
            ViewBag.skillId = new SelectList(db.Skills, "id", "name", achievedskill.SkillId);
            ViewBag.skillLevelId = new SelectList(db.SkillLevels, "id", "name", achievedskill.SkillLevelId);
            ViewBag.skillScoreId = new SelectList(db.SkillScores, "id", "name", achievedskill.skillScoreId);
            ViewBag.approvedById = new SelectList(db.Employees, "id", "firstName", achievedskill.ApprovedById);
            return View(achievedskill);
        }
        
        //
        // GET: /achievedSkill/Edit/5

        public virtual ActionResult Edit(int id)
        {
            AchievedSkill achievedskill = db.AchievedSkills.Find(id);
            ViewBag.employeeId = new SelectList(db.Employees, "id", "firstName", achievedskill.EmployeeId);
            ViewBag.skillId = new SelectList(db.Skills, "id", "name", achievedskill.SkillId);
            ViewBag.skillLevelId = new SelectList(db.SkillLevels, "id", "name", achievedskill.SkillLevelId);
            ViewBag.skillScoreId = new SelectList(db.SkillScores, "id", "name", achievedskill.skillScoreId);
            ViewBag.approvedById = new SelectList(db.Employees, "id", "firstName", achievedskill.ApprovedById);
            return View(achievedskill);
        }

        //
        // POST: /achievedSkill/Edit/5

        [HttpPost]
        public virtual ActionResult Edit(AchievedSkill achievedskill)
        {
            if (ModelState.IsValid)
            {
                db.Entry(achievedskill).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.employeeId = new SelectList(db.Employees, "id", "firstName", achievedskill.EmployeeId);
            ViewBag.skillId = new SelectList(db.Skills, "id", "name", achievedskill.SkillId);
            ViewBag.skillLevelId = new SelectList(db.SkillLevels, "id", "name", achievedskill.SkillLevelId);
            ViewBag.skillScoreId = new SelectList(db.SkillScores, "id", "name", achievedskill.skillScoreId);
            ViewBag.approvedById = new SelectList(db.Employees, "id", "firstName", achievedskill.ApprovedById);
            return View(achievedskill);
        }

        //
        // GET: /achievedSkill/Delete/5

        public virtual ActionResult Delete(int id)
        {
            AchievedSkill achievedskill = db.AchievedSkills.Find(id);
            return View(achievedskill);
        }

        //
        // POST: /achievedSkill/Delete/5

        [HttpPost, ActionName("Delete")]
        public virtual ActionResult DeleteConfirmed(int id)
        {            
            AchievedSkill achievedskill = db.AchievedSkills.Find(id);
            db.AchievedSkills.Remove(achievedskill);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}