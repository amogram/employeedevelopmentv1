﻿using System;
using System.Linq;
using System.Web.Mvc;
using Development.Models;
using Development.Services;
using Development.ViewModels;
using Development.Helpers;

namespace Development.Controllers
{
    public partial class EmployeeMenuController : Controller
    {
        private readonly ILogHoursService _logHoursService;
        private readonly IEmployeeService _employeeService;
        private readonly IVerificationService _verificationService;

        public EmployeeMenuController(ILogHoursService logHoursService, IEmployeeService employeeService, IVerificationService verificationService, IPresenterService presenterService)
        {
            _logHoursService = logHoursService;
            _employeeService = employeeService;
            _verificationService = verificationService;
        }

        [Authorize]
        public virtual ViewResult LogHours(int id = 0)
        {
            if(id==0) id = _employeeService.GetEmployeeId(User.Identity.Name);
            var model = _logHoursService.GetLogHours(id);
            return View(model);
        }

        [HttpPost]
        [Authorize]
        public virtual ActionResult LogHours(FormCollection formCollection)
        {

            var employeeId = _employeeService.GetEmployeeId(User.Identity.Name);

            var rows = int.Parse(formCollection["hdnRows"]);

            for (var i = 1; i <= rows; i++)
            {
                var log = new Log
                {
                    date = DateTime.Parse(formCollection["date"]),
                    employeeId = employeeId,
                    skillId = int.Parse(formCollection[String.Format("skill{0}", i)]),
                    skillLevelId = int.Parse(formCollection[String.Format("skilllevel{0}", i)]),

                    hours = DevelopmentHelper.ReturnDecimal(formCollection[String.Format("hour{0}", i)]),

                    unitsCompleted = DevelopmentHelper.ReturnIntOrNull(formCollection[String.Format("unit{0}", i)]),
                    percentRework = DevelopmentHelper.ReturnIntOrNull(formCollection[String.Format("rework{0}", i)]),
                    percentCoaching = DevelopmentHelper.ReturnIntOrNull(formCollection[String.Format("coaching{0}", i)]),

                    notes = formCollection[String.Format("note{0}", i)],

                    projectId = _employeeService.GetDefaultProjectId(employeeId)
                };

                _logHoursService.AddLogHours(log);
            }

            _logHoursService.Save();

            var model = _logHoursService.GetLogHours(employeeId);

            return PartialView(MVC.EmployeeMenu.Actions.Views.HoursPartial, model);
        }

        [Authorize]
        public virtual ActionResult GetLogHoursState(int id)
        {
            var result = _logHoursService.GetLogHoursState(id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public virtual PartialViewResult Search(DateTime date, int id)
        {
            return PartialView();
        }

        public virtual ActionResult VerifyHours()
        {
            var model = new VerifyHoursViewModel
            {
                Hours = _verificationService.GetAllHours().ToList(),
            };

            return View(model);
        }

        [HttpPost]
        [Authorize]
        public virtual ActionResult SearchVerifyHours(VerifyHoursViewModel model)
        {
            model.Hours = _verificationService.GetAllHours(model.VerifyHoursSearch.DateFrom, model.VerifyHoursSearch.DateTo).ToList();
            return View(MVC.EmployeeMenu.Views.VerifyHours, model);
            //return PartialView(MVC.EmployeeMenu.Views.VerifyHoursPartial, model);
        }

        [HttpPost]
        [Authorize]
        public virtual ActionResult VerifyHours(VerifyHoursViewModel model)
        {
            var toVerify = model.Hours.Where(x => x.IsVerified).ToList();
            
            foreach (var verifyHoursModel in toVerify)
            {
                var logs = 
                    (model.VerifyHoursSearch == null) ? 
                        _verificationService.GetNonVerifiedLogHours(verifyHoursModel.EmployeeId)
                            :_verificationService.GetNonVerifiedLogHours(verifyHoursModel.EmployeeId, model.VerifyHoursSearch.DateFrom, model.VerifyHoursSearch.DateTo);

                foreach (var log in logs)
                {

                    log.verifiedById = _employeeService.GetEmployeeId(User.Identity.Name);
                    log.IsVerified = true;

                    _logHoursService.Update(log);
                    _logHoursService.Save();
                }
            }

            return RedirectToAction(MVC.EmployeeMenu.Actions.VerifyHours());
        }

        [HttpPost]
        [Authorize]
        public virtual ActionResult VerifyHoursDetails(VerifyHoursDetailModel model)
        {
            foreach (var log in model.Logs)
            {
                var logToUpdate = _logHoursService.GetLog(log.id);
                logToUpdate.IsVerified = log.IsVerified;
                logToUpdate.verifiedById = _employeeService.GetEmployeeId(User.Identity.Name);

                if (logToUpdate.IsVerified)
                {
                    logToUpdate.verifiedHours = logToUpdate.hours;
                    logToUpdate.verifiedUnitsCompletd = logToUpdate.unitsCompleted;
                }
                else
                {
                    logToUpdate.verifiedHours = 0;
                    logToUpdate.verifiedUnitsCompletd = 0;
                }

                _logHoursService.Update(logToUpdate);
                _logHoursService.Save();
            }

            return RedirectToAction(MVC.EmployeeMenu.Actions.VerifyHours());
        }

        public virtual ActionResult VerifyHoursDetails(int id, DateTime? dateFrom, DateTime? dateTo, VerificationSearchType search = VerificationSearchType.Both)
        {
            dateFrom = dateFrom ?? new DateTime(1900, 1, 1);
            dateTo = dateTo ?? new DateTime(3000, 1, 1);

            var model = new VerifyHoursDetailModel
                            {
                               Logs = _verificationService.GetHours(id, dateFrom, dateTo, search),
                               IsVerified = search == VerificationSearchType.VerifiedHours
                            };

            return View(model);
        }
    }
}