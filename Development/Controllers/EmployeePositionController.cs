﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Development.Models;

namespace Development.Controllers
{
    public partial class EmployeePositionController : Controller
    {
        private DevelopmentContext db = new DevelopmentContext();

        //
        // GET: /EmployeePosition/

        public virtual ViewResult Index()
        {
            return View(db.EmployeePositions.ToList());
        }

        //
        // GET: /EmployeePosition/Details/5

        public virtual ViewResult Details(int id)
        {
            EmployeePosition employeeposition = db.EmployeePositions.Find(id);
            return View(employeeposition);
        }

        //
        // GET: /EmployeePosition/Create

        public virtual ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /EmployeePosition/Create

        [HttpPost]
        public virtual ActionResult Create(EmployeePosition employeeposition)
        {
            if (ModelState.IsValid)
            {
                db.EmployeePositions.Add(employeeposition);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(employeeposition);
        }
        
        //
        // GET: /EmployeePosition/Edit/5

        public virtual ActionResult Edit(int id)
        {
            EmployeePosition employeeposition = db.EmployeePositions.Find(id);
            return View(employeeposition);
        }

        //
        // POST: /EmployeePosition/Edit/5

        [HttpPost]
        public virtual ActionResult Edit(EmployeePosition employeeposition)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employeeposition).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(employeeposition);
        }

        //
        // GET: /EmployeePosition/Delete/5

        public virtual ActionResult Delete(int id)
        {
            EmployeePosition employeeposition = db.EmployeePositions.Find(id);
            return View(employeeposition);
        }

        //
        // POST: /EmployeePosition/Delete/5

        [HttpPost, ActionName("Delete")]
        public virtual ActionResult DeleteConfirmed(int id)
        {            
            EmployeePosition employeeposition = db.EmployeePositions.Find(id);
            db.EmployeePositions.Remove(employeeposition);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}