﻿using System;
using System.Web.Mvc;
using Development.Models;
using Development.Services;
using Development.ViewModels;

namespace Development.Controllers
{
    public partial class EmployeeWithDirectsController : Controller
    {
        private readonly IEmployeeService _employeeService;
        private readonly IPresenterService _presenterService;
        private readonly ISkillAchievementService _skillAchievementService;
        private readonly IRankAchievementService _rankAchievementService;

        public EmployeeWithDirectsController(IEmployeeService employeeService, IRankAchievementService rankAchievementService, 
            IPresenterService presenterService, ISkillAchievementService skillAchievementService)
        {
            _employeeService = employeeService;
            _presenterService = presenterService;
            _skillAchievementService = skillAchievementService;
            _rankAchievementService = rankAchievementService;
        }

        [Authorize]
        public virtual ActionResult Index()
        {
            var model = new SkillAchievementViewModel
                            {
                                AchievedSkill = new AchievedSkill(),
                                Presenter = new PresenterViewModel(_presenterService)
                            };

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public virtual ActionResult Index(SkillAchievementViewModel model, FormCollection formCollection)
        {
            model.Presenter = new PresenterViewModel(_presenterService);
            model.AchievedSkill.SkillId = int.Parse(formCollection["skill1"]);
            model.AchievedSkill.SkillLevelId = int.Parse(formCollection["skilllevel1"]);
            //var minHours = formCollection["tbMinhours"] != null ? int.Parse(formCollection["tbMinhours"]) : 0;

            model.PreviousSkills = _skillAchievementService.GetPreviousAchievedSkills(model.AchievedSkill.EmployeeId, model.AchievedSkill.SkillId, model.AchievedSkill.SkillLevelId);
            
            model.AchievedSkill.HoursSinceLastSkillAchievement =
                _skillAchievementService.HasLogHoursForAchievedSkill(model.AchievedSkill)
                    ? _skillAchievementService.GetHoursSinceLastAchievement(model)
                    : 0;

            model.AchievedSkill.EfficencyScore = _skillAchievementService.GetEfficencyScore(model);
            model.AchievedSkill.SixMonthEfficencyScore = _skillAchievementService.GetEfficencyScore(model, true);

            model.AchievedSkill.PreviousAbility = _skillAchievementService.GetCurrentAbility(model);

            return PartialView(MVC.EmployeeWithDirects.Views.SkillAchievementPartial, model);
        }
            
        [Authorize]
        [HttpPost]
        public virtual ActionResult AddSkillAchievement(SkillAchievementViewModel model)
        {
            model.AchievedSkill.date = DateTime.Now;
            model.AchievedSkill.ApprovedById = _employeeService.GetEmployeeId(User.Identity.Name);
            model.PreviousSkills = _skillAchievementService.GetPreviousAchievedSkills(model.AchievedSkill.EmployeeId, model.AchievedSkill.SkillId, model.AchievedSkill.SkillLevelId);
            model.AchievedSkill.AssociatedLogs = _skillAchievementService.GetAssociatedLogs(model);

            _skillAchievementService.AddAchievedSkill(model.AchievedSkill);
            _skillAchievementService.Save();

            return RedirectToAction(MVC.EmployeeWithDirects.Actions.Index());
        }

        public virtual ActionResult RankAchievement()
        {
            var model = new RankAchievementViewModel
                            {
                                Presenter = new PresenterViewModel(_presenterService)
                            };
            return View(model);
        }

        [HttpPost]
        [Authorize]
        public virtual ActionResult RankAchievement(RankAchievementViewModel model)
        {
            var acheivedRanks = _rankAchievementService.CalculateRank(model.EmployeeId, model.SkillLevelId);
            return View(MVC.EmployeeWithDirects.Views.RecommendedRankAchievement, acheivedRanks);
        }
    }
}
