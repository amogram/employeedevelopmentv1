﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Development.Models.DAL;

namespace Development.Models
{
    public class DevelopmentContext : DbContext
    {
        public DbSet<AchievedRank> AchievedRanks { get; set; }
        public DbSet<AchievedSkill> AchievedSkills { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeePosition> EmployeePositions { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Rank> Ranks { get; set; }
        public DbSet<RankRule> RankRules { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<SkillLevel> SkillLevels { get; set; }
        public DbSet<SkillLevelEntry> SkillLevelEntries { get; set; }
        public DbSet<SkillScore> SkillScores { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Verification> Verifications { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DevelopmentContext, Configuration>());

            modelBuilder.Entity<Employee>()
                        .HasRequired(a => a.employeePosition)
                        .WithMany()
                        .HasForeignKey(u => u.employeePositionId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

        }
    }
}