﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Development.Models
{
    public class AchievedRank
    {
        public int id { get; set; }
        public DateTime date { get; set; }
        public string notes { get; set; }
        //[Key, Column(Order = 0), Display, Editable(true)]
        public int employeeId { get; set; }
        //[Key, Column(Order = 1), Display, Editable(true)]
        public int rankId { get; set; }
        public int? approvedById { get; set; }
        public int? verifiedById { get; set; }
        [ForeignKey("employeeId")]
        public Employee employee { get; set; }
        public Rank rank { get; set; }
        [ForeignKey("approvedById")]
        public Employee approvedBy { get; set; }
        [ForeignKey("verifiedById")]
        public Employee verifiedBy { get; set; }
    }
}