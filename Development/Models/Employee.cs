﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Development.Models
{
    public class Employee
    {
        public int id { get; set; }

        [Required(ErrorMessage="*")]
        [Display(Name = "First Name")]
        public string firstName { get; set; }

        [Required(ErrorMessage="*")]
        [Display(Name = "Surname")]
        public string lastName { get; set; }

        [Required(ErrorMessage="*")]
        [Display(Name = "User Name")]
        public string username { get; set; }

        [Required(ErrorMessage="*")]
        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        //[DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string password { get; set; }

        [NotMapped]
        //[DataType(DataType.Password)]
        //[Display(Name = "Confirm password")]
        //[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string confirmPassword { get; set; }

        [Required(ErrorMessage="*")]
        [Display(Name = "Email")]
        public string email { get; set; }

        [Display(Name = "Employed Since")]
        public DateTime? employedSince { get; set; }

        [Display(Name = "Hourly Pay")]
        public decimal? hourlyPay { get; set; }

        [Display(Name = "Notes")]
        public string notes { get; set; }

        [Display(Name = "Default Project")]
        public int? defaultProjectId { get; set; }

        [Display(Name="Position")]
        public int employeePositionId { get; set; }

        [Display(Name = "Supervisor")]
        public int? supervisorId { get; set; }

        [ForeignKey("defaultProjectId")]
        public virtual Project defaultProject { get; set; }

        [ForeignKey("employeePositionId")]
        public virtual EmployeePosition employeePosition { get; set; }

        [ForeignKey("supervisorId")]
        public virtual Employee supervisor { get; set; }

        public virtual ICollection<AchievedRank> achieved_ranks { get; set; }
        //public virtual ICollection<AchievedRank> approved_ranks { get; set; }
        //public virtual ICollection<AchievedRank> verified_ranks { get; set; }
        public virtual ICollection<Log> logs { get; set; }
        //public virtual ICollection<Log> approved_logs { get; set; }
        //public virtual ICollection<Verification> verifications { get; set; }
        public virtual ICollection<AchievedSkill> achieved_skills { get; set; }
        //public virtual ICollection<AchievedSkill> approved_skills { get; set; }
        public virtual ICollection<Employee> directs { get; set; }

        public string fullName
        {
            get
            {
                return string.Format("{0} {1}", firstName, lastName);
            }
        }

        public static List<Employee> order(IEnumerable<Employee> employees)
        {
            return order(employees.ToList());
        }

        public static List<Employee> order(List<Employee> employees)
        {
            return employees.OrderBy(s => s.firstName).OrderBy(s => s.lastName).ToList();
        }
    }
}