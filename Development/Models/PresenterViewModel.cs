﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Development.Models
{
    public class PresenterViewModel
    {
        public PresenterViewModel(DevelopmentContext db, int projectId = 0)
        {
            Employees = new SelectList(Employee.order(db.Employees), "id", "fullName");
            Projects = new SelectList(Project.order(db.Projects), "id", "name", projectId);
            Skills = new SelectList(Skill.order(db.Skills), "id", "indentedName");
            SkillLevels = new SelectList(db.SkillLevels.ToList(), "id", "name");
            Tasks = new SelectList(db.Tasks.Where(x => x.projectId == projectId), "id", "name");
            SkillScores = new SelectList(db.SkillScores.ToList(), "id", "name");
        }

        public SelectList Employees { get; set; }
        public SelectList Projects { get; set; }
        public SelectList Skills { get; set; }
        public SelectList SkillLevels { get; set; }
        public SelectList Tasks { get; set; }
        public SelectList SkillScores { get; set; }
    }
}
