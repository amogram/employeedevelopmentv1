﻿using System.Data;
using System.Linq;
using Development.Models;
using Development.Services;
using Development.ViewModels;
using System.Data.Entity;
using System.Web.Mvc;

namespace Development.BLL
{
    /// <summary>
    /// Log Hours Service
    /// </summary>
    public class LogHoursService : ILogHoursService
    {
        readonly IDevelopmentContext db;
        private readonly IPresenterService _presenterService;

        public LogHoursService(IDevelopmentContext developmentContext, IPresenterService presenterService)
        {
            db = developmentContext;

            _presenterService = presenterService;
        }


        /// <summary>
        /// Retrieve all logged hours, total hours and total rework and coaching percentages for an employee.
        /// Also populates Presenter object for use in html controls.
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public LogViewModel GetLogHours(int employeeId)
        {
            var model = new LogViewModel
            {
                Logs = db.Logs.Where(s => s.employee.id.Equals(employeeId))
                    .Include(l => l.employee).Include(l => l.skillLevel).Include(l => l.skill).Include(l => l.verifiedBy),
                Presenter = new PresenterViewModel(_presenterService)
            };

            model.TotalHours = model.Logs.Sum(x => x.hours).Value;
            model.PercentageRework = model.TotalHours > 0 ? model.Logs.Sum(x => x.hours * x.percentRework).Value / model.TotalHours : 0;
            model.PercentageCocaching = model.TotalHours > 0 ? model.Logs.Sum(x => x.hours * x.percentCoaching).Value / model.TotalHours : 0;

            return model;
        }

        /// <summary>
        /// Add log hours
        /// </summary>
        /// <param name="log"></param>
        public void AddLogHours(Log log)
        {
            db.Logs.Add(log);   
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <returns></returns>
        public int Save()
        {
            return db.SaveChanges();
        }

        /// <summary>
        /// Determines if a particular skill is optional, required or disabled, based on holistic and individualistic values.
        /// Returns an object than can be passed backed to client in JSON format.
        /// </summary>
        /// <param name="skillId"></param>
        /// <returns></returns>
        public object GetLogHoursState(int skillId)
        {
            var skill = db.Skills.SingleOrDefault(x => x.id == skillId);
            var state = UnitCompletedState.Optional;

            if (skill.holisitic != null && skill.individualistic != null)
            {
                if (!skill.holisitic.Value && skill.individualistic.Value)
                {
                    state = UnitCompletedState.Required;
                }
                if (skill.holisitic.Value && !skill.individualistic.Value)
                {
                    state = UnitCompletedState.Disabled;
                }
            }

            var skillLevels =
                skill.skillLevelEntries.OrderBy(x => x.skillLevel.order).Select(x => new SelectListItem { Text = x.skillLevel.description, Value = x.skillLevel.id.ToString() }).ToList();

            return new
            {
                SkillLevels = skillLevels,
                State = state,
                Unit = skill.unitOfMeasurement
            };
        }

        /// <summary>
        /// Update Log
        /// </summary>
        /// <param name="log"></param>
        public void Update(Log log)
        {
            db.Entry(log).State = EntityState.Modified;
        }


        /// <summary>
        /// Retreive Log
        /// </summary>
        /// <param name="logId"></param>
        /// <returns></returns>
        public Log GetLog(int logId)
        {
            return db.Logs.SingleOrDefault(x => x.id == logId);
        }
    }
}