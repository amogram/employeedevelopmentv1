﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Development.Models;
using Development.Services;

namespace Development.BLL
{
    public class RankAchievementService : IRankAchievementService
    {
        readonly IDevelopmentContext db;

        public RankAchievementService(IDevelopmentContext developmentContext)
        {
            db = developmentContext;
        }

        /// <summary>
        /// Return collection of achieved ranks at particular skill level, based on employee and skill level.
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="skillLevelId"></param>
        /// <returns></returns>
        public IEnumerable<AchievedRank> CalculateRank(int employeeId, int skillLevelId)
        {
            var rankResults = new List<AchievedRank>();

            var achievedSkills = db.AchievedSkills.Where(x => x.EmployeeId == employeeId && x.SkillLevelId == skillLevelId).OrderByDescending(x => x.skillScore.scoreValue).ToList();

            if (achievedSkills.Count <= 0) return rankResults;

            var skills = Skill.order(db.Skills);

            var topLevelSkills = skills.Where(x => x.parentId == null).ToList();

            foreach (var topLevelSkill in topLevelSkills)
            {
                if (!achievedSkills.Any(x => x.skill.topLevelSkillId == topLevelSkill.id)) continue;

                var maxSkillScore =
                    achievedSkills.Where(x => x.skill.topLevelSkillId == topLevelSkill.id).Max(x => x.skillScore.scoreValue);

                if (maxSkillScore <= 0) continue;

                var score = GetRankScore(topLevelSkill, skills, achievedSkills, maxSkillScore);

                var rankRule =
                   topLevelSkill.rankRules.Where(x => x.skillId == topLevelSkill.id && x.skillLevelId == skillLevelId).ToList();

                foreach (var rule in rankRule)
                {
                    if (score >= rule.minScorePercentage)
                    {
                        var achievedRank = new AchievedRank
                        {
                            date = DateTime.Now,
                            employeeId = employeeId,
                            employee = db.Employees.SingleOrDefault(x => x.id == employeeId),
                            rankId = rule.rankId,
                            rank = db.Ranks.SingleOrDefault(x => x.id == rule.rankId)
                        };
                        rankResults.Add(achievedRank);
                    }
                }
            }

            return rankResults;
        }

        /// <summary>
        /// Recursive method that tranverses skill tree and works out score accordingly
        /// </summary>
        /// <param name="skill"></param>
        /// <param name="skills"></param>
        /// <param name="achievedSkills"></param>
        /// <param name="maxSkillScore"></param>
        /// <returns></returns>
        public decimal GetRankScore(Skill skill, IEnumerable<Skill> skills, IEnumerable<AchievedSkill> achievedSkills, decimal maxSkillScore)
        {
            decimal skillScore = 0;

            if (HasAchievedSkillAtThisLevel(achievedSkills, skill))
            {
                skillScore =
                    (achievedSkills.Where(x => x.SkillId == skill.id).Select(x => x.skillScore.scoreValue).
                         SingleOrDefault() / maxSkillScore) * 100;
            }

            if (HasChildren(skills, skill))
            {
                var children = skills.Where(x => x.parentId == skill.id).ToList();
                decimal childrenScore = 0;

                foreach (var child in children)
                {
                    childrenScore += GetRankScore(child, skills, achievedSkills, maxSkillScore);
                }

                var avergageChildrenScore = childrenScore / children.Count;

                if (skillScore == 0)
                {
                    return avergageChildrenScore;
                }
                else
                {
                    return skillScore >= avergageChildrenScore ? skillScore : avergageChildrenScore;
                }
            }

            return skillScore;

        }

        public bool HasAchievedSkillAtThisLevel(IEnumerable<AchievedSkill> achievedSkills, Skill skill)
        {
            return achievedSkills.Any(x => x.SkillId == skill.id);
        }

        public bool HasChildren(IEnumerable<Skill> skills, Skill skill)
        {
            return skills.Any(x => x.parentId == skill.id);
        }
    }
}