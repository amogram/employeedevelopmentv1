﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Development.Models;
using Development.Services;
using Development.ViewModels;

namespace Development.BLL
{
    /// <summary>
    /// CRUD functionality for achieved skills as well as calculating hours since last achivement and efficency.
    /// </summary>
    public class SkillAchievementService : ISkillAchievementService
    {
        readonly IDevelopmentContext db;
        
        public SkillAchievementService(IDevelopmentContext developmentContext)
        {
            db = developmentContext;
        }

        public void AddAchievedSkill(AchievedSkill achievedSkill)
        {
            db.AchievedSkills.Add(achievedSkill);
        }

        public int Save()
        {
            return db.SaveChanges();
        }

        public IList<AchievedSkill> GetPreviousAchievedSkills(int employeeId, int skillId, int skillLevelId)
        {
            return db.AchievedSkills.Where(x => x.EmployeeId == employeeId
                     && x.SkillLevelId == skillLevelId
                     && x.SkillId == skillId).OrderByDescending(x => x.date).ToList();
        }

        public decimal? GetHoursSinceLastAchievement(SkillAchievementViewModel model)
        {

            model.AchievedSkill.AssociatedLogs = GetAssociatedLogs(model);

            var hours = model.AchievedSkill.AssociatedLogs.Sum(x => x.hours);

            return hours;

        }

        /// <summary>
        /// Pulls all hours logged for a particular skill and skill level, for the employee.
        /// Compares that against previous skills where skill achievement already logged.
        /// REturn those Logs that are not assoicated with a skill.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ICollection<Log> GetAssociatedLogs(SkillAchievementViewModel model)
        {
            var allLogs = db.Logs.Where(x =>
                            x.employeeId == model.AchievedSkill.EmployeeId &&
                            x.skillLevelId == model.AchievedSkill.SkillLevelId &&
                            x.skillId == model.AchievedSkill.SkillId).ToList();

            var previousLogs = new List<Log>();

            foreach (var previousSkill in model.PreviousSkills)
            {
                if (previousSkill.AssociatedLogs != null)
                    previousLogs.AddRange(previousSkill.AssociatedLogs);
            }

            return allLogs.Except(previousLogs).ToList();
        }

        public bool HasLogHoursForAchievedSkill(AchievedSkill achievedSkill)
        {
            return db.Logs.Any(
                x =>
                x.employeeId == achievedSkill.EmployeeId && x.skillLevelId == achievedSkill.SkillLevelId &&
                x.skillId == achievedSkill.SkillId);
        }

        public decimal? GetCurrentAbility(SkillAchievementViewModel model)
        {
            return db.AchievedSkills.Where(
               x =>
               x.EmployeeId == model.AchievedSkill.EmployeeId && x.SkillLevelId == model.AchievedSkill.SkillLevelId &&
               x.SkillId == model.AchievedSkill.SkillId).OrderByDescending(x => x.date).Select(x => x.CurrentAbility).FirstOrDefault();

        }

        /// <summary>
        /// Calculates efficency score
        /// </summary>
        /// <param name="model"></param>
        /// <param name="isSixMonths"></param>
        /// <returns></returns>
        public decimal? GetEfficencyScore(SkillAchievementViewModel model, bool isSixMonths = false)
        {
            var sixMonthsAgo = new DateTime(1900, 1, 1);
            if (isSixMonths) sixMonthsAgo = DateTime.Now.AddMonths(-6);

            var totalVerifiedUnitsCompleted =
                db.Logs.Where(
                    x =>
                    x.employeeId == model.AchievedSkill.EmployeeId && x.skillLevelId == model.AchievedSkill.SkillLevelId &&
                    x.skillId == model.AchievedSkill.SkillId && x.date >= sixMonthsAgo).Sum(x => x.verifiedUnitsCompletd);

            var totalVerifiedHours = db.Logs.Where(
                x =>
                x.employeeId == model.AchievedSkill.EmployeeId && x.skillLevelId == model.AchievedSkill.SkillLevelId &&
                x.skillId == model.AchievedSkill.SkillId).Sum(x => x.verifiedHours);

            var employeeRate = totalVerifiedHours > 0 ? totalVerifiedUnitsCompleted / totalVerifiedHours : 0;

            var projectId = db.Employees.SingleOrDefault(x => x.id == model.AchievedSkill.EmployeeId).defaultProjectId;

            var task =
                db.Tasks.Where(
                    x => x.skillId == model.AchievedSkill.SkillId && x.projectId == projectId).
                    Select(x => new
                    {
                        scheduledHours = x.scheduledHours,
                        numOfUnits = x.numOfUnits
                    }).SingleOrDefault();

            var standard = task != null ? task.scheduledHours / task.numOfUnits : 1;

            var projectScore = employeeRate / standard;

            var efficencyScore = totalVerifiedHours > 0 ? projectScore * totalVerifiedHours / totalVerifiedHours : 0;

            return efficencyScore;

        }
    }
}