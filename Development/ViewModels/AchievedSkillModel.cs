﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Development.ViewModels
{
    public class AchievedSkillModel
    {
        [Display(Name = "Employee")]
        public int EmployeeId { get; set; }

        public int ProjectId { get; set; }

        [Display(Name = "Task")]
        public int SkillId { get; set; }

        [Display(Name = "Skill Level")]
        public int SkillLevelId { get; set; }

        [Display(Name = "Hours since last entry")]
        public decimal? HoursSinceLastSkillAchievement { get; set; }

        [Display(Name = "Previous Ability")]
        public int PreviousAbility { get; set; }

        [Display(Name = "Efficency Score")]
        public decimal? EfficencyScore { get; set; }

        public int MinHours { get; set; }

        public decimal? CurrentAbility { get; set; }

        public decimal? SixMonthEfficencyScore { get; set; }

        public decimal? EfficencyScoreOverride { get; set; }

        public bool HasLearnedElsewhere { get; set; }

        public string Notes { get; set; }
    }
}
