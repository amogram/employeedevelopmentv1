using System.Collections.Generic;
using Development.Models;

namespace Development.Services
{
    public interface IRankAchievementService
    {
        /// <summary>
        /// Return collection of achieved ranks at particular skill level
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="skillLevelId"></param>
        /// <returns></returns>
        IEnumerable<AchievedRank> CalculateRank(int employeeId, int skillLevelId);

        /// <summary>
        /// Recursive method that tranverses skill tree and works out score accordingly
        /// </summary>
        /// <param name="skill"></param>
        /// <param name="skills"></param>
        /// <param name="achievedSkills"></param>
        /// <param name="maxSkillScore"></param>
        /// <returns></returns>
        decimal GetRankScore(Skill skill, IEnumerable<Skill> skills, IEnumerable<AchievedSkill> achievedSkills, decimal maxSkillScore);

        bool HasAchievedSkillAtThisLevel(IEnumerable<AchievedSkill> achievedSkills, Skill skill);
        bool HasChildren(IEnumerable<Skill> skills, Skill skill);
    }
}